using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSpheres : MonoBehaviour
{
// Revealing fields in the inspector.
    [SerializeField]
    private GameObject SphereContainer;
    [SerializeField]
    private GameObject Sphere;
    [SerializeField]
    private Material redMaterial;
    [SerializeField]
    private List<GameObject> spheresMade;

    private float duration = 10;
    private float timer = 0;
    private float timeDiff = 0.25f;
    private float lastSpawnTime = 0;
    private bool spawning = false;
    private bool canClick = true;
    private float sphereSize = 0.5f;

    void Awake()
    {
        spheresMade = new List<GameObject>();
    }

    void Update()
    {
// Checking if the object was clicked so we can start spawning. Removing previously made spheres.
        if(Input.GetMouseButtonDown(0) && canClick)
        {
            removeSphere();
            canClick = false;
            spawning = true;
            timer = 0;
            lastSpawnTime = 0;
        }

// Tracking spawning time and creating spheres. 
        if(spawning)
        {
            timer += Time.deltaTime;

            if(timer <= duration && timer >= (lastSpawnTime + timeDiff))
            {
                CreateSphere();
            }
        }

// Resetting and turning spheres red along.
        if(timer > duration)
        {
            timer = 0;
            lastSpawnTime = 0;
            TurnRed();
            spawning = false;
        }
    }

// Destroy all sphere objects, clearing out the list, and enabling.
    private void removeSphere()
    {
        for(int i = 0; i < spheresMade.Count; i++)
        {
            Destroy(spheresMade[i]);
        }

        spheresMade.Clear();
    }

// Creating spheres if they will not intersect. 
    private void CreateSphere()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

// Checking if ray hit something.
        if(Physics.Raycast(ray, out hit, 100.0f))
        {
            if(hit.transform != null)
            {
                bool canCreate = true;
// Making sure the new sphere is atleast 0.5f distance away from all others. 
                for(int i = 0; i < spheresMade.Count; i++)
                {
                    float distance = Mathf.Abs(Vector3.Distance(spheresMade[i].transform.position, hit.point));
                    if(distance <= sphereSize + 0.1f)
                    {
                        canCreate = false;
                        break;                         
                    }
                }

// Create sphere and add it to the list of spheres created.
                if(canCreate)
                {
                    GameObject obj = Instantiate(Sphere, hit.point + new Vector3(0, sphereSize/2, 0), new Quaternion(0,0,0,0));
                    obj.transform.parent = SphereContainer.transform;
                    spheresMade.Add(obj);
                    lastSpawnTime = timer;
                }
            }
        }
    }

// Changes the material to a red material.
    private void TurnRed()
    {
        for(int i = 0; i < spheresMade.Count; i++)
        {
            spheresMade[i].GetComponent<MeshRenderer>().material = redMaterial;
        }

        canClick = true;
        print(spheresMade.Count + " Sphere created");
    }
}


